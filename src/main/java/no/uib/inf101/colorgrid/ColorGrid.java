package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;


public class ColorGrid implements IColorGrid{
  // TODO: Implement this class DONE!


//Instansvariabel som kan lagre ting
    private List<List<Color>> grid; 


    int cols;
    int rows;

  public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    this.grid = new ArrayList<>();
    for (int i = 0; i < rows; i++){
      this.grid.add(new ArrayList<Color>());
      for (int j = 0; j < cols; j++){
        this.grid.get(i).add(null);
      }
    }

  }


  @Override
  public int rows() {
    return rows;
    // DONE
  }

  @Override
  public int cols() {
    // DONE
    return cols;
  }

  @Override
  public List<CellColor> getCells() {
    ArrayList<CellColor> gridCells = new ArrayList<>();
    for (int i = 0; i < grid.size(); i++){
      for (int j = 0; j < grid.get(0).size(); j++){
        CellPosition pos = new CellPosition(i, j); 
        CellColor gridCellColor = new CellColor(pos, grid.get(i).get(j));
        gridCells.add(gridCellColor); 
      }
    }
    return gridCells;
  }

  @Override
  public Color get(CellPosition pos) {
    return this.grid.get(pos.row()).get(pos.col());
    
  }

  @Override
  public void set(CellPosition pos, Color color) {
    // TODO Auto-generated method stub
    // 2 variabler - Hent ut int row og cols fra pos 
    int getRowsPos = pos.row(); 
    int getColsPos = pos.col();
      this.grid.get(getRowsPos).set(getColsPos, color); 
    }
    
  }


