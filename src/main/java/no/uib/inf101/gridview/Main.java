package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;
import java.awt.Color;

import javax.swing.JFrame;


public class Main {
  public static void main (String[] args) {
    ColorGrid gridC = new ColorGrid(3, 4);
    gridC.set(new CellPosition(0, 0), Color.RED);
    gridC.set(new CellPosition(0, 3), Color.BLUE);
    gridC.set(new CellPosition(2, 0), Color.YELLOW);
    gridC.set(new CellPosition(2, 3), Color.GREEN);

    GridView canvas = new GridView(gridC);
    JFrame frame = new JFrame();
    frame.setTitle("INF101");
    frame.setContentPane(canvas);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
}
