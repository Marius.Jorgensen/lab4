package no.uib.inf101.gridview;

import java.awt.Dimension;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Graphics;

import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.geom.Rectangle2D;



public class GridView extends JPanel{
  // Her ligger instansvariablene mine
  
  private IColorGrid gridC; 
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
  private static final double INNERMARGIN = 30;

  public GridView(IColorGrid gridC){ //Konstruktøren --- Parameter:(IColorGrid gridC)

    this.setPreferredSize(new Dimension(400, 300));
    this.gridC = gridC;

  }


  @Override 
  public void paintComponent(Graphics g){
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;     
    drawGrid(g2); 
    
  }

  public void drawGrid(Graphics2D g2){
    double outerWidth = this.getWidth() -(2 * OUTERMARGIN);
    double outerHeight = this.getHeight() -(2 * OUTERMARGIN);
    Rectangle2D box = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, outerWidth, outerHeight);
    g2.setColor(MARGINCOLOR);
    g2.fill(box);

    CellPositionToPixelConverter cpToPix = new CellPositionToPixelConverter(box, gridC, INNERMARGIN);
    drawCells(g2, gridC, cpToPix);
    }

    private static void drawCells(Graphics2D g2, CellColorCollection ccc, CellPositionToPixelConverter cpToPixCon){
      for (CellColor cc : ccc.getCells()){
        Rectangle2D cell = cpToPixCon.getBoundsForCell(cc.cellPosition());
        if (cc.color()==null){
          g2.setColor(Color.DARK_GRAY);
        }
        else {
          g2.setColor(cc.color());
        }
        g2.fill(cell);
      }
    }
  }


